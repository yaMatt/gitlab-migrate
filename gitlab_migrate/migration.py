from gitlab import GitLab
from time import sleep

def get_session(server, token):
    return GitLab(server, private_token=token)


def get_all_projects(session):
    return session.projects.list(all=True)


def archive_project(project):
    project.archive()


def export_project(project, destination):
    export = project.exports.create({})

    # Wait for the 'finished' status
    export.refresh()
    while export.export_status != 'finished':
        sleep(1)
        export.refresh()

    # Download the result
    with open(destination, 'wb') as f:
        export.download(streamed=True, action=f.write)


def import_project(session, owner, project_name, source_file):
    output = session.projects.import_project(source_file, project_name, owner)

    # Get a ProjectImport object to track the import status
    project_import = session.projects.get(output['id'], lazy=True).imports.get()
    while project_import.import_status != 'finished':
        sleep(1)
        project_import.refresh()
