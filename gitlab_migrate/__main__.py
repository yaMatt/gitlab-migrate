from os import path
from argparse import ArgumentParser
from . import migration

def project_to_path(base, project):
    return path.join(base, project.path_with_namespace + ".tgz")


def get_args():
    parser = ArgumentParser(description='Migrate GitLab projects')
    parser.add_argument("server_url", help="URL to GitLab instance")
    parser.add_argument("token", help="Access token")
    parser.add_argument(
        "destination_dir",
        help="What directory to store the exports",
        default="."
    )
    return parser.parse_args()


def export_projects(args):
    session = migration.get_session(args.server_url, args.token)
    projects = migration.get_all_projects(session)
    for project in projects:
        destination_dir = project_to_path(args.destination_dir, project)
        migration.export_project(project, destination_dir)
        migration.archive_project(project)


#def import_projects(args):
    # find and open archived files
    # find owner and project
#    session = migration.get_session(args.server_url, args.token)
#    migration.import_project(session, owner, project_name, f)


if __name__ == "__main__":
    args = get_args()
    if args.export:
        export_projects(args)
    else:
        import_projects(args)
